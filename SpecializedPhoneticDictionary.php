<?php
	class SpecializedPhoneticDictionary
	{ 
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $language = '';
		private $phoneme_attributes_array = array();
		private $unknown_phonemes = array();
		private $replacement_pairs = array();
		private $get = array();
		private $result = '';
		const LETTERS_RUS_UPPER = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ';
		const LETTERS_RUS_LOWER = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';
		const BR = "<br>\n";
		
		function __construct()
		{
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			$this->readPhonemeAttributes();
		}
		
		public static function loadLanguages()
		{
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files))
			{
				foreach($files as $file)
				{
					if(substr($file, 2, 4) == '.txt')
					{
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang)
		{
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false)
			{
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line)
			{
				if(empty($line))
				{
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//')
				{
					if(empty($key))
					{
						$key = $line;
					}
					else
					{
						if(!isset(self::$localizationArr[$key]))
						{
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg)
		{
			if(isset(self::$localizationArr[$msg]))
			{
				return self::$localizationArr[$msg];
			}
			else
			{
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang)
		{
			if(!empty(self::$localizationErr))
			{
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Specialized Phonetic Dictionary';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/SpecializedPhoneticDictionary/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		public function setLanguage($lang)
		{
			$this->language = $lang;
		}
		
		private function readPhonemeAttributes()
		{
			$filePath = dirname(__FILE__) . "/base/phoneme_attributes.txt";
			$fp = fopen($filePath, 'r') OR die('fail open file "phoneme_attributes.txt"');
			if($fp)
			{
				while(($line = fgets($fp, 4096)) !== false)
				{
					if(substr($line, 0, 2) != "//")
					{
						$phoneme_attributes = preg_split("/\t/", $line);
						$phoneme_attributes[0] = trim($phoneme_attributes[0]);
						$this->phoneme_attributes_array[$phoneme_attributes[0]][] = $phoneme_attributes[1];
						$this->phoneme_attributes_array[$phoneme_attributes[0]][] = $phoneme_attributes[2];
						$this->phoneme_attributes_array[$phoneme_attributes[0]][] = $phoneme_attributes[3];
						$this->phoneme_attributes_array[$phoneme_attributes[0]][] = $phoneme_attributes[4];
						$this->phoneme_attributes_array[$phoneme_attributes[0]][] = $phoneme_attributes[5];
						$this->phoneme_attributes_array[$phoneme_attributes[0]][] = $phoneme_attributes[6];
						$this->phoneme_attributes_array[$phoneme_attributes[0]][] = $phoneme_attributes[7];
						$this->phoneme_attributes_array[$phoneme_attributes[0]][] = $phoneme_attributes[8];
					}
				}
			}
			fclose($fp);
		}
		
		private function ordutf8($string)
		{
			$offset = 0;
			$code = ord(substr($string, $offset, 1));
			if($code >= 128)											//otherwise 0xxxxxxx
			{
				if($code < 224) $bytesnumber = 2;						//110xxxxx
				elseif($code < 240) $bytesnumber = 3;					//1110xxxx
				elseif($code < 248) $bytesnumber = 4;					//11110xxx
				$codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
				for ($i = 2; $i <= $bytesnumber; $i++)
				{
					$offset++;
					$code2 = ord(substr($string, $offset, 1)) - 128;	//10xxxxxx
					$codetemp = $codetemp*64 + $code2;
				}
				$code = $codetemp;
			}
			$offset += 1;
			if($offset >= strlen($string)) $offset = -1;
			$codehex = strtoupper(dechex($code));
			if(strlen($codehex) == 1) return "U+000$codehex";
			elseif(strlen($codehex) == 2) return "U+00$codehex";
			elseif(strlen($codehex) == 3) return "U+0$codehex";
			elseif(strlen($codehex) == 4) return "U+$codehex";
			else return $codehex;
		}
		
		private function getUnicodeChar($code)
		{
			$code = str_replace('U+', '0x', $code);
			if(function_exists('mb_convert_encoding'))
			{
				return mb_convert_encoding('&#'.intval($code, 0).';', 'UTF-8', 'HTML-ENTITIES');
			}
			else
			{
				return chr(intval($code));
			}
		}
		
		private function getFirstLetters($examples)
		{
			$first_letters_arr = array();
			$wordforms_cnt = 0;
			
			$filePath = dirname(__FILE__) . "/base/paradigms.txt";
			$fp = fopen($filePath, 'r') OR die('fail open file "paradigms.txt"');
			if($fp)
			{
				$previous_line = '';
				while(($line = fgets($fp, 4096)) !== false)
				{
					$line = trim($line);
					if(!empty($line))
					{
						$first_letter_upper = mb_strtoupper(mb_substr($line, 0, 1));
						if(mb_strpos(self::LETTERS_RUS_UPPER, $first_letter_upper) !== false)
						{
							$wordforms_cnt++;
							if(strpos($previous_line, '+') !== false)
							{
								if($examples == 'show') $first_letters_arr[$first_letter_upper][] = substr($line, 0, strpos($line, ' '));
								if($examples == 'hide') $first_letters_arr[$first_letter_upper] = 1;
							}
						}
					}
					$previous_line = $line;
				}
			}
			fclose($fp);
			
			ksort($first_letters_arr);
			
			$result = '';
			if($examples == 'show')
			{
				foreach($first_letters_arr as $letter => $lexemes)
				{
					$result .= "<a href=index.php?lang=" . $this->language . "&op=getLexemes&letter=$letter>$letter</a> (" . implode(", ", $lexemes) . ")" . self::BR;
				}
			}
			if($examples == 'hide')
			{
				$result .= '<table><tr height=40>';
				$i = 0;
				foreach($first_letters_arr as $letter => $value)
				{
					$i++;
					if($i > 6)
					{
						$result .= '</tr><tr height=40>';
						$i = 1;
					}
					$result .= "<td width=48><a href=index.php?lang=" . $this->language . "&op=getLexemes&letter=$letter class='symbol-button'>$letter</a></td>";
				}
				$result .= '</tr></table>';
			}
			$result .= self::BR . self::BR . self::$localizationArr['total wordforms'] . ': '. $wordforms_cnt;
			return $result;
		}
		
		private function getLexemes($letter)
		{
			$lexemes_arr = array();
			
			$filePath = dirname(__FILE__) . "/base/paradigms.txt";
			$fp = fopen($filePath, 'r') OR die('fail open file "paradigms.txt"');
			if($fp)
			{
				$previous_line = '';
				while(($line = fgets($fp, 4096)) !== false)
				{
					$line = trim($line);
					if(!empty($line))
					{
						if(strpos($previous_line, '+') !== false && mb_strtoupper(mb_substr($line, 0, 1)) == $letter)
						{
							$lexeme_stressed = substr($line, 0, strpos($line, ' '));
							$lexeme_nonstressed = str_replace('̀', '', str_replace('́', '', $lexeme_stressed));
							$lexemes_arr[$lexeme_nonstressed] = $lexeme_stressed;
						}
					}
					$previous_line = $line;
				}
			}
			fclose($fp);
			
			ksort($lexemes_arr);
			
			$result = '';
			foreach($lexemes_arr as $lexeme)
			{
				$result .= "<a href=index.php?lang=" . $this->language . "&op=getParadigm&letter=$letter&lexeme=$lexeme>$lexeme</a>" . self::BR;
			}
			return $result;
		}
		
		private function getParadigm($letter, $lexeme)
		{
			$result = '';
			$filePath = dirname(__FILE__) . "/base/paradigms.txt";
			$fp = fopen($filePath, 'r') OR die('fail open file "paradigms.txt"');
			if($fp)
			{
				$previous_line = '';
				$saving_line = false;
				while(($line = fgets($fp, 4096)) !== false)
				{
					$line = trim($line);
					if(strpos($previous_line, '+') !== false)
					{
						if(substr($line, 0, strpos($line, ' ')) == $lexeme)
						{
							$saving_line = true;
							$result .= '<table><tr><td>';
						}
						else
						{
							$saving_line = false;
						}
					}
					if($saving_line == true && strpos($line, '*') !== false)
					{
						$line = str_replace ('*', '</td></tr><tr><td>', $line);
					}
					if($saving_line == true && substr($line, 0, 2) != '//' && strpos($line, '+') === false)
					{
						$wordform = trim(substr($line, 0, strpos($line, '{')));
						$wordform = str_replace(' ', '_', $wordform);
						if(preg_match_all("/\[[^\]]+\]/u", $line, $transcriptions_in_line) != 0)
						{
							foreach($transcriptions_in_line[0] as $transcription)
							{
								$transcription_res = $this->makeIndexes($transcription);
								$transcription_res = preg_replace("/\s*,\s*/", '', $transcription_res);
								$transcription_without_spaces = preg_replace("/\s*,\s*/", ',', $transcription);
								$line = str_replace($transcription, "<a href=index.php?lang=" . $this->language . "&op=getPhoneticParsing&letter=$letter&lexeme=$lexeme&wordform=$wordform&transcription=$transcription_without_spaces>$transcription_res</a>", $line);
							}
						}
						$result .= $line . self::BR;
					}
					$previous_line = $line;
				}
				$result .= "</td></tr></table>";
			}
			fclose($fp);
			return $result;
		}
		
		private function makeIndexes($transcription)
		{
			$transcription_tmp = trim($transcription, '[]');
			$phonemes = preg_split("/\s*,\s*/", $transcription_tmp);
			foreach($phonemes as $phoneme)
			{
				$pattern = '/(.*[а-я].*)([а-я].*)/u';
				preg_match_all($pattern, $phoneme, $phoneme_parts, PREG_SET_ORDER);
				if(!empty($phoneme_parts))
				{
					$phoneme1 = '[' . $phoneme . ']';
					if(isset($this->phoneme_attributes_array[$phoneme1]))
					{
						if(strpos($phoneme_parts[0][1], 'ц') !== false && strpos($phoneme_parts[0][2], 'с') !== false)
						{
							$transcription = str_replace($phoneme, $phoneme_parts[0][1] . '<sup>' . $phoneme_parts[0][2] . '</sup>', $transcription);
						}
						elseif($this->phoneme_attributes_array[$phoneme1][0] == 'согласный')
						{
							$transcription = str_replace($phoneme, '<sup>' . $phoneme_parts[0][1] . '</sup>' . $phoneme_parts[0][2], $transcription);
						}
						elseif($this->phoneme_attributes_array[$phoneme1][0] == 'гласный')
						{
							$transcription = str_replace($phoneme, $phoneme_parts[0][1] . '<sup>' . $phoneme_parts[0][2] . '</sup>', $transcription);
						}
					}
				}
			}
			return $transcription;
		}
		
		private function getPhoneticParsing($wordform, $transcription)
		{
			$result = '';
			$transcription_res = $this->makeIndexes($transcription);
			$transcription_res = str_replace(',', '', $transcription_res);
			$wordform = str_replace('_', ' ', $wordform);
			$result .=  $wordform . ' ' . $transcription_res . self::BR . self::BR;
			$transcription = trim($transcription, "[]");
			$phonemes = explode(',', $transcription);
			
			$result .= '<table>';
			foreach($phonemes as $phoneme)
			{
				$result .= '<tr><td>';
				$phoneme = "[$phoneme]";
				$phoneme_indexed = $this->makeIndexes($phoneme);
				if(isset($this->phoneme_attributes_array[$phoneme]))
				{
					$phoneme_discription = trim(trim(implode(', ', array_filter($this->phoneme_attributes_array[$phoneme]))), ',');
					$result .= $phoneme_indexed . " - " . $phoneme_discription . self::BR;
				}
				elseif($phoneme == '[‿]')
				{
					$result .= self::BR;
					continue;
				}
				else
				{
					$result .= '<font color="red">' . $phoneme_indexed . "</font> - " . self::BR;
				}
				$result .= '</td></tr>';
			}
			$result .= '</table>';
			return $result;
		}
		
		private function checkUnknownPhonemes()
		{
			$unknown_phonemes_arr = array();
			
			$filePath = dirname(__FILE__) . '/base/paradigms.txt';
			$fp = fopen($filePath, 'r') OR die('fail open file "paradigms.txt"');
			if($fp)
			{
				while(($line = fgets($fp, 4096)) !== false)
				{
					preg_match_all("/\[([^\]]+)\]/u", $line, $matches);
					foreach($matches[1] as $match)
					{
						$phonemes = preg_split("/\s*,\s*/", $match);
						foreach($phonemes as $phoneme)
						{
							if(!empty($phoneme) && !isset($this->phoneme_attributes_array["[$phoneme]"]) && $phoneme != '‿')
							{
								$unknown_phonemes_arr[$phoneme][] = $line;
							}
						}
					}
				}
			}
			fclose($fp);
			
			$result = '<b>UNKNOWN PHONEMES</b> (' . count($unknown_phonemes_arr) . '):' . self::BR . self::BR;
			$result .= '<table>';
			foreach($unknown_phonemes_arr as $unknown_phoneme => $examples)
			{
				$result .= '<tr><td valign="top"><font color="red"><b>' . "[$unknown_phoneme]" . "</b></font></td><td>";
				foreach($examples as $example)
				{
					$example = str_replace($unknown_phoneme, '<font color="red">' . $unknown_phoneme . '</font>', $example);
					$result .= "- $example" . self::BR;
				}
				$result .=  self::BR . '</td></tr>';
			}
			$result .= '</table>';
			return $result;
		}
		
		private function getAllSymbols($filename)
		{
			$all_symbols_arr = array();
			$filePath = dirname(__FILE__) . '/base/' . $filename;
			$fp = fopen($filePath, 'r') OR die('fail open base file');
			if($fp)
			{
				echo "<b>ALL USED SYMBOLS</b> (<i>$filename</i>):" . self::BR . self::BR;
				while(($line = fgets($fp, 4096)) !== false)
				{
					$chars = preg_split('//u', $line, -1, PREG_SPLIT_NO_EMPTY);
					foreach($chars as $char)
					{
						$char_code = $this->ordutf8($char);
						if(!isset($all_symbols_arr[$char_code]))
						{
							$all_symbols_arr[$char_code]['char'] = $char;
							$all_symbols_arr[$char_code]['line'] = $line;
							$all_symbols_arr[$char_code]['count'] = 1;
						}
						else $all_symbols_arr[$char_code]['count']++;
					}
				}
			}
			fclose($fp);

			array_multisort($all_symbols_arr);
			
			$result = '';
			$result .= '<table>';
			foreach($all_symbols_arr as $code => $val)
			{
				$val['line'] = str_replace($val['char'], '<font color="red">' . $val['char'] . '</font>', $val['line']);
				$result .=  '<tr valign="top">
								<td width=36><font color="red">' . $val['char'] . '</font></td>
								<td width=76>' . $code . '</td>
								<td width=60>' . $val['count'] . '</td>
								<td>' . $val['line'] . '</td>
							</tr>';
			}
			$result .=  '</table>' . self::BR;
			$result .=  'THE TOTAL NUMBER OF DIFFERENT SYMBOLS USED IS: ' . count($all_symbols_arr);
			return $result;
		}
		
		private function replaceCharacters($filename)
		{
			$filePath = dirname(__FILE__) . '/base/replacements.txt';
			$fp = fopen($filePath, 'r') OR die('fail open base file');
			if($fp)
			{
				while(($line = fgets($fp, 4096)) !== false)
				{
					if(substr($line, 0, 2) != "//")
					{
						$replacement_pair = preg_split("/\s*\->\s*/", $line);
						$replace_arr = array();
						$search_arr = array();
						if(!empty($replacement_pair))
						{
							if(isset($replacement_pair[1]))
							{
								$search_arr = preg_split("/,/", $replacement_pair[0]);
							}
							if(isset($replacement_pair[1]))
							{
								$replace_arr = preg_split("/,/", $replacement_pair[1]);
							}
							$search_res = '';
							$replace_res = '';
							foreach($search_arr as $search) $search_res .= $this->getUnicodeChar($search);
							foreach($replace_arr as $replace) $replace_res .= $this->getUnicodeChar($replace);
							$this->replacement_pairs['search'][] = $search_res;
							$this->replacement_pairs['replace'][] = $replace_res;
						}
					}
				}
			}
			fclose($fp);

			$filePath = dirname(__FILE__) . '/base/' . $filename;
			$file_contents = file_get_contents($filePath) OR die('fail open base file');
			$new_contents = str_replace($this->replacement_pairs['search'], $this->replacement_pairs['replace'], $file_contents, $count);
			echo "Было выканана $count замен(ы).";
			file_put_contents($filePath, $new_contents);
		}
		
		public function run($get)
		{
			$this->get = $get;
			$op = $get['op'];
			$ex = $get['ex'];
			$letter = $get['letter'];
			$lexeme = $get['lexeme'];
			$wordform = $get['wordform'];
			$transcription = $get['transcription'];
			
			if(!isset($ex)) $ex = 'hide';
			
			if('getFirstLetters' == $op || empty($op))
			{
				if('hide' == $ex)
				{
					$this->result .= '<table width=100%><tr><td width=40%>' . $this->getFirstLetters($ex) . '</td>';
					$this->result .= '<td width=60% valign="top"><h3><a href=index.php?lang=' . $this->language . '&op=getFirstLetters&ex=show>' . self::$localizationArr['show examples'] . '</a><br />';
					if('80.94.171.87' == $_SERVER["REMOTE_ADDR"] or '127.0.0.1' == $_SERVER['REMOTE_ADDR'])
						$this->result .= '<a href=index.php?lang=' . $this->language . '&op=checkUnknownPhonemes>Праверыць на памылкі</a><br /><a href=index.php?lang=' . $this->language . '&op=getAllSymbols>Паказаць усе выкарыстаныя сімвалы</a><br /><a href=index.php?lang=' . $this->language . '&op=replaceCharacters>Замяніць сімвалы</a></h3></td>';
					$this->result .= '</tr></table>';
				}
				if('show' == $ex)
				{
					$this->result .= $this->getFirstLetters($ex);
					$this->result .= '<div class="divider"></div><a href=index.php?lang=' . $this->language . '&op=getFirstLetters&ex=hide>' . self::$localizationArr['hide examples'] . '</a><br />';
					if('80.94.171.87' == $_SERVER["REMOTE_ADDR"] or '127.0.0.1' == $_SERVER['REMOTE_ADDR'])
						$this->result .= '<a href=index.php?lang=' . $this->language . '&op=checkUnknownPhonemes>Праверыць на памылкі</a><br /><a href=index.php?lang=' . $this->language . '&op=getAllSymbols>Паказаць усе выкарыстаныя сімвалы</a><br /><a href=index.php?lang=' . $this->language . '&op=replaceCharacters>Замяніць сімвалы</a>';
				}
			}
			elseif('getLexemes' == $op)
			{
				$this->result .= $this->getLexemes($letter);
				$this->result .= '<div class="divider"></div>';
				$this->result .= '<a href=index.php?lang=' . $this->language . '>' . self::$localizationArr['main page'] . "</a>";
			}
			elseif('getParadigm' == $op)
			{
				$this->result .= $this->getParadigm($letter, $lexeme);
				$this->result .= '<div class="divider"></div>';
				$this->result .= '<a href=index.php?lang=' . $this->language . '>' . self::$localizationArr['main page'] . "</a> → <a href=index.php?lang=" . $this->language . "&op=getLexemes&letter=$letter>$letter</a>";
			}
			elseif('getPhoneticParsing' == $op)
			{
				$this->result .= $this->getPhoneticParsing($wordform, $transcription);
				$this->result .= '<div class="divider"></div>';
				$this->result .= '<a href=index.php?lang=' . $this->language . '>' . self::$localizationArr['main page'] . "</a> → <a href=index.php?lang=" . $this->language . "&op=getLexemes&letter=$letter>$letter</a> → <a href=index.php?lang=" . $this->language . "&op=getParadigm&letter=$letter&lexeme=$lexeme>$lexeme</a>";
			}
			elseif('checkUnknownPhonemes' == $op)
			{
				$this->result .= $this->checkUnknownPhonemes();
				$this->result .= '<div class="divider"></div>';
				$this->result .= '<a href=index.php?lang=' . $this->language . '>' . self::$localizationArr['main page'] . '</a>';
			}
			elseif('getAllSymbols' == $op)
			{
				$this->result .= $this->getAllSymbols('paradigms.txt');
				$this->result .= '<br /><br /><div class="divider"></div><br /><br />';
				$this->result .= $this->getAllSymbols('phoneme_attributes.txt');
				$this->result .= '<div class="divider"></div>';
				$this->result .= '<a href=index.php?lang=' . $this->language . '>' . self::$localizationArr['main page'] . '</a>';
			}
			elseif('replaceCharacters' == $op)
			{
				$this->result .= $this->replaceCharacters('paradigms.txt');
				$this->result .= '<br /><br /><div class="divider"></div><br /><br />';
				$this->result .= $this->replaceCharacters('phoneme_attributes.txt');
				$this->result .= '<div class="divider"></div>';
				$this->result .= '<a href=index.php?lang=' . $this->language . '>' . self::$localizationArr['main page'] . '</a>';
			}
		}
		
		private function getUrl()
		{
			$url = @($_SERVER["HTTPS"] != 'on') ? 'http://'. $_SERVER["SERVER_NAME"] : 'https://' . $_SERVER["SERVER_NAME"];
			$url .= ($_SERVER["SERVER_PORT"] != 80) ? ':' . $_SERVER["SERVER_PORT"] : '';
			$url .= $_SERVER["REQUEST_URI"];
			$url = urldecode($url);
			return $url;
		}
		
		public function saveCacheFiles()
		{
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$date_code = date('Y-m-d_H-i-s', time());
			$rand_code = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			$serviceName = 'SpecializedPhoneticDictionary';
			$sendersName = 'Specialized Phonetic Dictionary';
			$senders_email = 'corpus.by@gmail.com';
			$recipient = 'corpus.by@gmail.com';
			$subject = "Specialized Phonetic Dictionary from IP $ip";
			$mail_body = "Вітаю, гэта corpus.by!" . self::BR;
			$mail_body .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$mail_body .= "URL: <a href=" . $this->getUrl() . ">" . $this->getUrl() . "</a>" . self::BR . self::BR;
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			$cache_text = preg_replace("/(^\s+)|(\s+$)/us", "", $this->getUrl());
			fwrite($new_file, $cache_text);
			fclose($new_file);
			
			$mail_body .= "URL захаваны тут: " . dirname(dirname(__FILE__)) . "/showCache.php?s=SpecializedPhoneticDictionary&t=in&f=$filename" . self::BR . self::BR;
			
			$mail_body .= "<table>";
			if(isset($this->get['op'])) $mail_body .= "<tr><td>Аперацыя</td><td>" . $this->get['op'] . "</td></tr>";
			if(isset($this->get['letter'])) $mail_body .= "<tr><td>Літара</td><td>" . $this->get['letter'] . "</td></tr>";
			if(isset($this->get['lexeme'])) $mail_body .= "<tr><td>Лексема</td><td>" . $this->get['lexeme'] . "</td></tr>";
			if(isset($this->get['wordform'])) $mail_body .= "<tr><td>Словаформа</td><td>" . $this->get['wordform'] . "</td></tr>";
			if(isset($this->get['transcription'])) $mail_body .= "<tr><td>Транскрыпцыя</td><td>" . $this->get['transcription'] . "</td></tr>";
			$mail_body .= "</table>";
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <$senders_email>\r\n";
			if($rand_code == 576)
			{
				mail($recipient, $subject, $mail_body, $header);
			}
			
			$filename = $date_code . '_' . $ip . '_' . $rand_code . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mail_body, $header)));
			fclose($newFile);
		}
		
		public function getResult()
		{
			return $this->result;
		}
	}
?>