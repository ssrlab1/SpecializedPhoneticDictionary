<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'SpecializedPhoneticDictionary.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = SpecializedPhoneticDictionary::loadLanguages();
	SpecializedPhoneticDictionary::loadLocalization($lang);
	$_GET['ex'] = !empty($_GET['ex']) ? $_GET['ex'] : 'hide';
	$op = isset($_GET['op']) ? $_GET['op'] : null;
	$ex = isset($_GET['ex']) ? $_GET['ex'] : null;
	$letter = isset($_GET['letter']) ? $_GET['letter'] : null;
	$lexeme = isset($_GET['lexeme']) ? $_GET['lexeme'] : null;
	$wordform = isset($_GET['wordform']) ? $_GET['wordform'] : null;
	$transcription = isset($_GET['transcription']) ? $_GET['transcription'] : null;
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo SpecializedPhoneticDictionary::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			$(document).ready(function () {
				$.ajax({
					type: 'GET',
					url: 'https://corpus.by/SpecializedPhoneticDictionary/api.php',
					data: {
						'localization': '<?php echo $lang; ?>',
						'op': '<?php echo $op; ?>',
						'ex': '<?php echo $ex; ?>',
						'letter': '<?php echo $letter; ?>',
						'lexeme': '<?php echo $lexeme; ?>',
						'wordform': '<?php echo $wordform; ?>',
						'transcription': '<?php echo $transcription; ?>'
					},
					success: function(msg) {
						var result = jQuery.parseJSON(msg);
						$('#resultId').html('<div class="sub-caption-smaller">' + result.result + '</div>');
					},
					error: function(){
						$('#resultId').html('ERROR');
					}
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/SpecializedPhoneticDictionary/?lang=<?php echo $lang; ?>"><?php echo SpecializedPhoneticDictionary::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo SpecializedPhoneticDictionary::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo SpecializedPhoneticDictionary::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = SpecializedPhoneticDictionary::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . SpecializedPhoneticDictionary::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<form method="post" action="">
					<p id="resultId"></p>
				</form>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo SpecializedPhoneticDictionary::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/SpecializedPhoneticDictionary" target="_blank"><?php echo SpecializedPhoneticDictionary::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/SpecializedPhoneticDictionary/-/issues/new" target="_blank"><?php echo SpecializedPhoneticDictionary::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo SpecializedPhoneticDictionary::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo SpecializedPhoneticDictionary::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo SpecializedPhoneticDictionary::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php SpecializedPhoneticDictionary::sendErrorList($lang); ?>