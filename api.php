<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	
	$localization = isset($_GET['localization']) ? $_GET['localization'] : 'en';
	$op = isset($_GET['op']) ? $_GET['op'] : null;
	$ex = isset($_GET['ex']) ? $_GET['ex'] : null;
	$letter = isset($_GET['letter']) ? $_GET['letter'] : null;
	$lexeme = isset($_GET['lexeme']) ? $_GET['lexeme'] : null;
	$wordform = isset($_GET['wordform']) ? $_GET['wordform'] : null;
	$transcription = isset($_GET['transcription']) ? $_GET['transcription'] : null;
	$arr = array('op' => $op, 'ex' => $ex, 'letter' => $letter, 'lexeme' => $lexeme, 'wordform' => $wordform, 'transcription' => $transcription);
	
	include_once 'SpecializedPhoneticDictionary.php';
	SpecializedPhoneticDictionary::loadLocalization($localization);
	
	$msg = '';
	$SpecializedPhoneticDictionary = new SpecializedPhoneticDictionary();
	$SpecializedPhoneticDictionary->setLanguage($localization);
	$SpecializedPhoneticDictionary->run($arr);
	$SpecializedPhoneticDictionary->saveCacheFiles();
	
	$result['result'] = $SpecializedPhoneticDictionary->getResult();
	$msg = json_encode($result);	
	echo $msg;
?>