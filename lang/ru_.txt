title
Специализированный фонетический словарь

help
https://ssrlab.by/ru/5692

be
Беларуская

ru
Русский

en
English

total wordforms
Всего словоформ

show examples
Показать примеры

hide examples
Спрятать примеры

total wordforms
Всего словоформ

main page
Главная страница

service code
Исходный код сервиса можно скачать по

reference
ссылке

suggestions
Предложить улучшение работы сервиса

contact e-mail
Мы будем рады получить обратную связь от Вас на e-mail

other prototypes
Другие наши прототипы:

laboratory
Лаборатория распознавания и синтеза речи, ОИПИ НАН Беларуси
